#!/bin/sh
# Put program names (that exist on /bin) into ./jails and run the below cmd to sandbox them
# accroding to their firejail profiles located in /etc or overridden in .config

cat ./jails |grep -v '#' | xargs -I% sudo ln -s /usr/bin/firejail /usr/local/bin/%
