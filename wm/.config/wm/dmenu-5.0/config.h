/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;		/* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
/*"DejaVu Sans Mono:size=9"*/
static const char *fonts[] =
  { "Inconsolata Nerd Font Mono:style=Regular:size=13:antialias=true" };
static const char *prompt = NULL;	/* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
  /*     fg         bg       */
  [SchemeNorm] = {"#DFD2C9", "#010006"},
  [SchemeSel] = {"#FFC9DB", "#DD4991"},
  [SchemeOut] = {"#D6AC94", "#D6AC94"},
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
