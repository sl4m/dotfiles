/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx = 1;	/* border pixel of windows */
static const unsigned int gappx = 10;	/* gaps between windows */
static const unsigned int snap = 32;	/* snap pixel */
static const int swallowfloating = 0;	/* 1 means swallow floating windows by default */
static const int showbar = 1;	/* 0 means no bar */
static const int topbar = 1;	/* 0 means bottom bar */
static const char *fonts[] = { "Inconsolata Nerd Font Mono:size=13:antialias:true" };
static const char dmenufont[] = "Inconsolata Nerd Font Mono:size=13:antialias:true";
//static const char col_gray1[] = "#171717";
static const char col_gray1[] = "#00040b";
static const char col_gray2[] = "#444444";
static const char col_gray3[] = "#bbbbbb";
static const char col_gray4[] = "#eeeeee";
/* 8b07af || 28194f || */
static const char dark_magenta[] = "#51022f";
//static const char dark_magenta[] = "#012d7e";
static const char neon_pink[] = "#e50082";
static const char *colors[][3] = {
  /*               fg         bg         border   */
  [SchemeNorm] = {col_gray3, col_gray1, col_gray2},
  [SchemeSel] = {col_gray4, dark_magenta, neon_pink},
};

/* tagging "",*/
static const char *tags[] =
  //{ "", "www", "dev", "", "", "", "" };
  { "󰣇", "2", "3", "4", "5", "6", "7" };


static const char shell[] = "/usr/bin/zsh";

static const Rule rules[] = {
  /* xprop(1):
   *      WM_CLASS(STRING) = instance, class
   *      WM_NAME(STRING) = title
   */
  /* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
  {"Gimp", NULL, NULL, 0, 1, 0, 0, -1},
  {"LibreWolf", NULL, NULL, 1 << 1, 0, 0, -1, -1},
  {"qBittorrent", NULL, NULL, 1 << 6, 0, 0, -1, -1},
  {"Virt-manager", NULL, NULL, 1 << 3, 0, 0, -1, -1},
  {"URxvt", NULL, NULL, 0, 0, 1, 0, -1},
  {"gnome-calculator", NULL, NULL, 0, 1, 0, 0, -1},
  {"Display", NULL, NULL, 0, 1, 0, 0, -1},
  {NULL, NULL, "Event Tester", 0, 0, 0, 1, -1},	/* xev */
};

/* layout(s) */
static const float mfact = 0.55;	/* factor of master area size [0.05..0.95] */
static const int nmaster = 1;	/* number of clients in master area */
static const int resizehints = 1;	/* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
  /* symbol     arrange function */
  {"[]=", tile},		/* first entry is default */
  {"><>", NULL},		/* no layout function means floating behavior */
  {"[M]", monocle},
};

/* key definitions */
#define XF86PowerOff			0x1008ff2a
#define XF86AudioMute 			0x1008ff12
#define XF86AudioLowerVolume		0x1008ff11
#define XF86AudioRaiseVolume		0x1008ff13
#define XF86MonBightnessDown		0x1008ff03
#define XF86MonBightnessUp		0x1008ff02
#define	Print				0xff61
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0";	/* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] =
  { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf",
col_gray3, "-sb", dark_magenta, "-sf", col_gray4, NULL };
//static const char *termcmd[]  = { "urxvtc","-sl","10000",NULL };
static const char *termcmd[] = { "alacritty", NULL };
static const char *nnn[] =
  { "alacritty", "-e", "bash", "-i", "-c", "n;bash", NULL };

static const char *cmdsoundup[] = { "pamixer", "-i", "5", NULL };
static const char *cmdsounddown[] = { "pamixer", "-d", "5", NULL };
static const char *cmdsoundtoggle[] = { "pamixer", "-t", NULL };
static const char *pulsemixer[] = { "alacritty", "-e", "pulsemixer", NULL };

static const char *passmenu[] = { "passmenu", NULL };
static const char *clipmenu[] = { "clipmenu", "-l", "5", "-i", NULL };
static const char *select_screenshot[] = { "sht", "select", NULL };
static const char *screenshot[] = { "sht", "full", NULL };
static const char *runscript[] = { "/home/sl4m/.local/bin/runsc.sh", NULL };

static const char *lockscreen[] = { "/home/sl4m/.local/bin/scripts/lock", NULL };
static const char *brightUp[] =
  { "bash", "/home/sl4m/.local/bin/scripts/backlight_status.sh", "i", NULL };
static const char *brightDown[] =
  { "bash", "/home/sl4m/.local/bin/scripts/backlight_status.sh", "d", NULL };

static const char *nextTrack[] =
  { "/home/sl4m/.local/bin/scripts/player.sh", "n", NULL };
static const char *previousTrack[] =
  { "/home/sl4m/.local/bin/scripts/player.sh", "p", NULL };
static const char *playPauseTrack[] =
  { "/home/sl4m/.local/bin/scripts/player.sh", "t", NULL };
static const char *loopTrack[] =
  { "/home/sl4m/.local/bin/scripts/player.sh", "l", NULL };

static const char *scratchpad[] =
  { "/home/sl4m/.local/bin/scripts/scratchpad.sh", NULL };

/*static const char *xcompmgrToggle[]={"/bin/bash","/home/sl4m/.local/bin/scripts/xcompToggle",NULL};*/

#include "shiftview.c"
static Key keys[] = {
  /* modifier                     key        function        argument */
  {MODKEY, XK_p, spawn, {.v = dmenucmd}},
  {MODKEY, XK_Return, spawn, {.v = termcmd}},
  {MODKEY, XK_b, togglebar, {0}},
  {MODKEY, XK_j, focusstack, {.i = +1}},
  {MODKEY, XK_k, focusstack, {.i = -1}},
  {MODKEY, XK_i, incnmaster, {.i = +1}},
  {MODKEY, XK_d, incnmaster, {.i = -1}},
  {MODKEY, XK_h, setmfact, {.f = -0.05}},
  {MODKEY, XK_l, setmfact, {.f = +0.05}},
  {MODKEY | ShiftMask, XK_Return, zoom, {0}},
  {MODKEY, XK_Tab, view, {0}},
  {MODKEY | ShiftMask, XK_q, killclient, {0}},
  {MODKEY | ShiftMask, XK_t, setlayout, {.v = &layouts[0]}},
  {MODKEY | ShiftMask, XK_f, setlayout, {.v = &layouts[1]}},
  {MODKEY | ShiftMask, XK_m, setlayout, {.v = &layouts[2]}},
  {MODKEY, XK_space, setlayout, {0}},
  {MODKEY | ShiftMask, XK_space, togglefloating, {0}},
  {MODKEY, XK_f, togglefullscr, {0}},
  {MODKEY, XK_0, view, {.ui = ~0}},
  {MODKEY | ShiftMask, XK_0, tag, {.ui = ~0}},
  {MODKEY, XK_comma, focusmon, {.i = -1}},
  {MODKEY, XK_period, focusmon, {.i = +1}},
  {MODKEY | ShiftMask, XK_comma, tagmon, {.i = -1}},
  {MODKEY | ShiftMask, XK_period, tagmon, {.i = +1}},
  {MODKEY, XK_minus, setgaps, {.i = -1}},
  {MODKEY, XK_equal, setgaps, {.i = +1}},
  {MODKEY | ShiftMask, XK_equal, setgaps, {.i = 0}},
  {MODKEY, XK_w, shiftview, {.i = +1}},
  {MODKEY, XK_q, shiftview, {.i = -1}},
  {0, XF86PowerOff, spawn, {.v = lockscreen}},

  {0, XF86MonBightnessUp, spawn, {.v = brightUp}},
  {0, XF86MonBightnessDown, spawn, {.v = brightDown}},

  {0, XF86AudioMute, spawn, {.v = cmdsoundtoggle}},
  {0, XF86AudioRaiseVolume, spawn, {.v = cmdsoundup}},
  {0, XF86AudioLowerVolume, spawn, {.v = cmdsounddown}},

  {MODKEY, XK_r, spawn, {.v = runscript}},
  {MODKEY, XK_m, focusmaster, {0}},
  {0, Print, spawn, {.v = screenshot}},
  {MODKEY, Print, spawn, {.v = select_screenshot}},
  {MODKEY, XK_F9, spawn, {.v = loopTrack}},
  {MODKEY, XK_F10, spawn, {.v = previousTrack}},
  {MODKEY, XK_F11, spawn, {.v = playPauseTrack}},
  {MODKEY, XK_F12, spawn, {.v = nextTrack}},
  {MODKEY, XK_n, spawn, {.v = nnn}},
  {MODKEY, XK_v, spawn, {.v = pulsemixer}},
  {MODKEY, XK_c, spawn, {.v = clipmenu}},
  {MODKEY, XK_grave, spawn, {.v = passmenu}},
  {MODKEY, XK_t, spawn, {.v = scratchpad}},

  TAGKEYS (XK_1, 0)
    TAGKEYS (XK_2, 1)
    TAGKEYS (XK_3, 2)
    TAGKEYS (XK_4, 3)
    TAGKEYS (XK_5, 4)
    TAGKEYS (XK_6, 5) TAGKEYS (XK_7, 6) {MODKEY | ShiftMask, XK_p, quit, {0}},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click                event mask      button          function        argument */
  {ClkLtSymbol, 0, Button1, setlayout, {0}},
  {ClkLtSymbol, 0, Button3, setlayout, {.v = &layouts[2]}},
  {ClkWinTitle, 0, Button2, zoom, {0}},
  {ClkStatusText, 0, Button2, spawn, {.v = termcmd}},
  {ClkClientWin, MODKEY, Button1, movemouse, {0}},
  {ClkClientWin, MODKEY, Button2, togglefloating, {0}},
  {ClkClientWin, MODKEY, Button3, resizemouse, {0}},
  {ClkTagBar, 0, Button1, view, {0}},
  {ClkTagBar, 0, Button3, toggleview, {0}},
  {ClkTagBar, MODKEY, Button1, tag, {0}},
  {ClkTagBar, MODKEY, Button3, toggletag, {0}},
};
