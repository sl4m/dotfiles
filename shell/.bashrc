[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

. `which env_parallel.bash`

. /usr/share/fzf/key-bindings.bash

eval "$(zoxide init bash)"

# rm -rf * .txt <=== pain

source "$HOME/.config/shell/aliasrc"

# Disable Ctrl s/q
stty -ixon

# Auto CD by typing
shopt -s autocd

# Vi mode
#set -o vi

batdiff () {
    git diff --name-only --diff-filter=d | xargs bat --diff
}


h () {
    $@ -h || $@ -H || $@ --help || $@ --h || $@ -help \
        || help $@ || man $@ || "$BROWSER" "http://www.startpage.com/search?q=$@";
}

n () {
    if [ -n "$NNNLVL" ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    nnn -Tv -Udxe "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            /bin/rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

_rm() {
    if is_mount >/dev/null 2>&1;then
        rm "$@"
    else
        trash-put -d "$@"
    fi
}

ex () {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: ex <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
 else
    if [ -f $1 ] ; then
        # NAME=${1%.*}
        # mkdir $NAME && cd $NAME
        case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.tar.xz)    tar xvJf $1    ;;
          *.lzma)      unlzma $1      ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       unrar x -ad $1 ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *.xz)        unxz $1        ;;
          *.exe)       cabextract $1  ;;
          *)           echo "extract: '$1' - unknown archive method" ;;
        esac
    else
        echo "$1 - file does not exist"
    fi
 fi
}
# Color for man pages, less ...
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

export _JAVA_AWT_WM_NONREPARENTING=1
export HISTSIZE=
export HISTFILESIZE=
export HISTCONTROL=ignorespace:ignoreboth:erasedups
export LESSHISTFILE=/dev/null
export EDITOR=/bin/nvim
#export TERMINAL=/bin/urxvtc
export TERMINAL=/bin/alacritty
export BROWSER=/bin/librewolf
export QT_QPA_PLATFORMTHEME=qt5ct

export _ZO_EXCLUDE_DIRS="$HOME/HDD/*:/mnt/*"

export PASSWORD_STORE_CLIP_TIME=25
#nnn config
export NNN_COLORS="5136"
export NNN_PLUG='v:imgview;F:fzopen;f:fzcd;z:autojump;b:setbg;t:preview-tui;p:preview-tabbed;g:bookmarks;c:cleanfilename;u:upload;e:vim_p;l:!git log;M:!exiftool $nnn'
export NNN_FIFO='/tmp/nnn.fifo'
export NNN_FCOLORS='c1e2812e006033f7c6d6abc4'
export NNN_TRASH=1
###
export FZF_DEFAULT_OPTS='--color=bg+:#51022f,spinner:regular,hl:#ff0000,prompt:#e50082'

#ask for sudo passwd using dmenu with password patch
export SUDO_ASKPASS=$HOME/.local/bin/scripts/askpass.sh

if [ "$TERM" = "rxvt-unicode-256color" ];then
    export PS1="\$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/')\[$(tput sgr0)\]\[\033[38;5;89m\]\W\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;33m\] \[$(tput sgr0)\]"
else
    export STARSHIP_CONFIG=~/.config/starship/starship.toml
    eval "$(starship init bash)"
fi
