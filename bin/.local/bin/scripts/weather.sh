#!/bin/bash
eval "$(argc --argc-eval "$0" "$@")"
# @describe weather forecast from terminal
# @option -l    location (Default is IP based)

if [ "$argc_l" ]; then
    city="$argc_l"
else
    city=$(curl -s https://ipinfo.io/json | jq .region)
fi
curl -fsSL https://wttr.in/"$city"
