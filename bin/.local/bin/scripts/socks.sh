#!/bin/sh
set -eu

resolve() {
    dig +short "$1"
}

update() {
    cd "$HOME/.config/mlvdmenu/misc" && ./update_proxies.sh || exit 1
    exit 0
}

proxy() {
    if systemctl status tor.service | grep -q running; then
        >&2 echo "$(tput setaf 125)Proxying Through Tor$(tput setaf 7)"
        proxychains -q "$@"
    else
        if [ "$L" ]; then
            proxy_host=$(shuf "$HOME/.config/mlvdmenu/misc/wg_socks5_hosts.txt" | grep -m1 "$L")
        else
            proxy_host=$(shuf -n1 "$HOME/.config/mlvdmenu/misc/wg_socks5_hosts.txt")
        fi
        proxy_ip=$(resolve "$proxy_host")

        loc=$(curl -x "socks5://$proxy_ip:1080" -s https://ipv4.am.i.mullvad.net/json | jq -r '.country+"/"+.city+"::"+.ip')
        >&2 echo "$(tput setaf 202)Proxying Through Mullvad $loc $(tput setaf 7)"

        echo "# proxychains.conf  VER 4.x
        strict_chain
        proxy_dns
        remote_dns_subnet 224
        tcp_read_time_out 15000
        tcp_connect_time_out 5000
        [ProxyList]
        socks5 $proxy_ip 1080"  > /tmp/socks.conf

        proxychains -q -f /tmp/socks.conf "$@"
    fi
}

case "$1" in
    "update")
        update
        ;;
    *)
        proxy "$@"
        ;;
esac
