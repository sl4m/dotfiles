#!/bin/sh

# https://github.com/LukeSmithxyz/voidrice/blob/master/.local/bin/dmenurecord

# Usage:
# `$0`: Ask for recording type via dmenu
# `$0 screencast`: Record both audio and screen
# `$0 video`: Record only screen
# `$0 audio`: Record only audio
# `$0 kill`: Kill existing recording
#
# If there is already a running instance, user will be prompted to end it.

killrecording() {
    if  [ -f /tmp/slip_record.pid ];then
        slip -q
        exit
    else
        recpid="$(cat /tmp/recordingpid)"
        # kill with SIGTERM, allowing finishing touches.
        kill -15 "$recpid"
        rm -f /tmp/recordingpid
        # even after SIGTERM, ffmpeg may still run, so SIGKILL it.
        sleep 3
        kill -9 "$recpid"
        exit
    fi

}

screencast() { \
	slop -f "%x %y %w %h" > /tmp/slop
	read -r X Y W H < /tmp/slop
	rm /tmp/slop

	ffmpeg -y \
	-f x11grab \
	-framerate 60 \
    -video_size "$W"x"$H" \
	-i :0.0+"$X,$Y" \
	-f alsa -i default \
	-r 30 \
 	-c:v h264 -crf 0 -preset ultrafast -c:a aac \
    -threads 4 \
	"$HOME/Pictures/vids/screencast-$(date '+%y%m%d-%H%M-%S').mp4" &
	echo $! > /tmp/recordingpid
       	}

video() { ffmpeg \
	-f x11grab \
	-s "$(xdpyinfo | awk '/dimensions/ {print $2;}')" \
	-i "$DISPLAY" \
 	-c:v libx264 -qp 0 -r 30 \
    -threads 4 \
	"$HOME/Pictures/vids/video-$(date '+%y%m%d-%H%M-%S').mkv" &
	echo $! > /tmp/recordingpid
	}

webcamhidef() { ffmpeg \
	-f v4l2 \
	-i /dev/video0 \
	-video_size 1920x1080 \
    -threads 4 \
	"$HOME/Pictures/vids/webcam-$(date '+%y%m%d-%H%M-%S').mkv" &
	echo $! > /tmp/recordingpid
	}

webcam() { ffmpeg \
	-f v4l2 \
	-i /dev/video0 \
	-video_size 640x480 \
    -threads 4 \
	"$HOME/Pictures/vids/webcam-$(date '+%y%m%d-%H%M-%S').mkv" &
	echo $! > /tmp/recordingpid
	}


audio() { \
	ffmpeg \
	-f alsa -i default \
	-c:a flac \
    -threads 4 \
	"$HOME/Pictures/vids/audio-$(date '+%y%m%d-%H%M-%S').flac" &
	echo $! > /tmp/recordingpid
	}

askrecording() { \
	choice=$(printf "screencast\\nvideo\\nvideo selected\\naudio\\nwebcam\\nwebcam (hi-def)\\ngif" | dmenu -i -p "Select recording style:")
	case "$choice" in
		screencast) screencast;;
		audio) audio;;
		video) video;;
        gif) screentogif;;
		*selected) videoselected;;
		webcam) webcam;;
		"webcam (hi-def)") webcamhidef;;
	esac
	}

asktoend() { \
	response=$(printf "No\\nYes" | dmenu -i -p "Recording still active. End recording?") &&
	[ "$response" = "Yes" ] &&  killrecording
	}

videoselected()
{
	slop -f "%x %y %w %h" > /tmp/slop
	read -r X Y W H < /tmp/slop
	rm /tmp/slop

	ffmpeg \
	-f x11grab \
	-framerate 60 \
	-video_size "$W"x"$H" \
	-i :0.0+"$X,$Y" \
	-c:v libx264 -qp 0 -r 30 \
    -preset ultrafast \
    -threads 4 \
	"$HOME/Pictures/vids/box-$(date '+%y%m%d-%H%M-%S').mkv" &
	echo $! > /tmp/recordingpid
}

screentogif() {
    slip -g
}

case "$1" in
	screencast) screencast;;
	audio) audio;;
	video) video;;
	*selected) videoselected;;
	gif) screentogif;;
	kill) killrecording;;
	*) [ -f /tmp/recordingpid ] || [ -f /tmp/slip_record.pid ] && asktoend  || askrecording;;
esac
