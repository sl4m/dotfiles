#!/bin/bash
# Inspired by https://github.com/kb-elmo/cavapify/blob/master/playcheck.sh
# Works with ncspot
# sets a cava config and corsair (with ckb-next) keyboard color same as the album art dominant color

img="$HOME/.cache/ncspot/covers/$(basename $(playerctl --player=ncspot metadata mpris:artUrl) )"

while [ ! -e $img ];do echo not found;done
echo found

numcol=6
fuzz=30
hex="#FFFFFF"

while [[ "$hex" == "#FFFFFF" && "$fuzz" != "60" ]]
do
    thresh=$((100-fuzz))
    sortedfinalcolors=$(convert "$img" -scale 50x50! -depth 8 \
    \( -clone 0 -colorspace HSB -channel gb -separate +channel -threshold $thresh% \
    -compose multiply -composite \) \
    -alpha off -compose copy_opacity -composite sparse-color:- |\
    convert -size 50x50 xc: -sparse-color voronoi '@-' \
    +dither -colors $numcol -depth 8 -format "%c" histogram:info: |\
    sed -n 's/^[ ]*\(.*\):.*[#]\([0-9a-fA-F]*\) .*$/\1,#\2/p' | sort -r -n -k 1 -t ",")
    hex=`echo "$sortedfinalcolors" | head -n 1 | cut -d, -f2`
    ((fuzz+=10))
done

sed -i "s/foreground.*/foreground = \'$hex\'/" $HOME/.config/cava/config3
echo rgb "$(echo $hex |tr -d '#')"FF >/tmp/ckbpipe000

pkill -USR2 cava >/dev/null
exit 0
