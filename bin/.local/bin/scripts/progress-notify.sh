#!/usr/bin/env sh
set -eu

# progress-notify - Send progress notifications (ex: audio and brightness) for dunst

# dependencies: dunstify

### How to use: ###
# Pass the values via stdin and provide the notification Text and icon to display as arguments.

### Ex: Brightness notifications ###
#   xbacklight -inc 5  && xbacklight -get | notify brightness brightness.png
#   xbacklight -dec 5  && xbacklight -get | notify brightness brightness.png

notify_progress() {

        progress="$1"
        if [ "$progress" -eq 0 ]; then
                dunstify -h string:x-canonical-private-synchronous:brightness "$text: " -h int:value:"$progress" -t 1500 --icon "$icon"
        elif [ "$progress" -le 30 ]; then
                dunstify -h string:x-canonical-private-synchronous:brightness "$text: " -h int:value:"$progress" -t 1500 --icon "$icon"
        elif [ "$progress" -le 70 ]; then
                dunstify -h string:x-canonical-private-synchronous:brightness "$text: " -h int:value:"$progress" -t 1500 --icon "$icon"
        else
                dunstify -h string:x-canonical-private-synchronous:brightness "$text: " -h int:value:"$progress" -t 1500 --icon "$icon"
        fi
}

input=`cat /dev/stdin`
text="$1"
icon="$2"

notify_progress "$input" "$text" "$icon"
