#!/bin/sh
set -eu
killall nc &

nc -lnvp "$1" &
curl -fLs https://am.i.mullvad.net/port/"$1"
