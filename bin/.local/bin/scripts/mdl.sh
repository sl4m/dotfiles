#!/bin/bash

cmd=${1-"yt-dlp"}
>&2 echo "Using $cmd"

vipe | parallel -j4 "$cmd" {} 2>/dev/null
