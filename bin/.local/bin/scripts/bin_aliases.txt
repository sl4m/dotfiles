## Link scripts to local bin
# awk '!/^#/{print "ln -sf $HOME/.local/bin/scripts/"$1" /usr/local/bin/"$2}'  bin_aliases.txt
##
http http
lemmino.sh lemmino
linkhandler lh
lock lock
lyrics.sh lyrics
mktest.sh mktest
myip.sh myip
pfcheck.sh pfchk
qr.sh qr
randagent.sh randagent
runx.sh runx
setbg.sh setbg
todo.sh td
tft.sh tft
weather.sh weather
yt-music.sh yt-music
yt-searchplay.sh yt-snp
socks.sh socks
screenshot sht
find_simage.sh czkawka-img
cargo-ssearch.sh cargo-ssearch
player.sh player
gen_favico.sh favico-gen
thumbnailer.sh sx-th
dock.sh dock
switch_zone.sh switch_zone
cht-lite.sh cht.sh
sw.sh sw
share_wifi.sh wish
mictest.sh mictest
is_mount.sh is_mount
mdl.sh mdl
audio_notify.sh notify-audio
fsl.sh fsl
cargo-pkgs cargo-pkgs
