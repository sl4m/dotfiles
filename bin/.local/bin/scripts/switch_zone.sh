#!/bin/bash
set -eu

zone=$(cd /usr/share/zoneinfo/ && fzf)

if [ "$EUID" -eq 0 ];then
    echo Setting /usr/share/zoneinfo/$zone to /etc/localtime
    ln -sf /usr/share/zoneinfo/$zone /etc/localtime
else
    echo [!] Failed: run as root
fi
