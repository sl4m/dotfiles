#!/bin/bash
# Use headless chromium to take WebPage screenshots

CHROMIUM_PATH="${CHROMIUM_PATH:-/bin/brave}"

# TODO handle args
# option to save on a created domain named dir :10

W_WIDTH="${W:-1920}"
W_HEIGHT="${H:-3240}"

URL="$1"

domain="$(unfurl domains < <(echo "$URL"))"
path="$(unfurl paths < <(echo "$URL") | tr '/' '_')"

# mkdir -p "${PWD}/${domain}"

$CHROMIUM_PATH --headless=new --no-gpu --hide-scrollbars \
    --fullpage --window-size="${W_WIDTH},${W_HEIGHT}" \
    "$URL" --screenshot="${domain}${path}_${W_WIDTH}x${W_HEIGHT}.png"
