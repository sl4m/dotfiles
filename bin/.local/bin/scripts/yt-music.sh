#!/bin/sh
set -eu

squery="$*"
mpv --no-resume-playback --vid=no ytdl://ytsearch:"$squery"
