#!/bin/sh
# Generate mullvad's wireguard proxies for importing into firefox extension foxyproxy

curl -s https://api.mullvad.net/www/relays/all/ | \
    jq -r  '.[] | select(.type == "wireguard") | "socks5://\(.socks_name):1080?cc=\(.country_code)&title=\(.hostname)"'
