#!/usr/bin/env sh
set -eu

_current="$(iwgetid -r)"

con=${1:-$_current}

passwd="$(nmcli -s -g 802-11-wireless-security.psk connection show "$con")"

notify-send -i nm-signal-100 "Wifi Sharing" "Sharing $con password $passwd"

qr "WIFI:S:$con;T:WPA;P:$passwd;;"
