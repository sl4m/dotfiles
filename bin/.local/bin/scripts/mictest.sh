#!/usr/bin/env sh
set -eu

file="/tmp/mic_test_$(date +%D-%H_%M_%S).out"

echo "Saving Under $file"
parecord -vv --format=dat "$file"
