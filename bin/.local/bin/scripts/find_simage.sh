#!/bin/sh
set -eu

czkawka-cli image -d "$PWD" -f results.txt
tail -n +5 results.txt |grep -io '^.*\(jpg\|png\)'|sxiv -
rm results.txt
