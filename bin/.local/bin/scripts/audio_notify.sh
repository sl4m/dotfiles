#!/usr/bin/env sh
set -eu

setsid mpv --really-quiet --script-opts=vmode=off --keep-open=no --loop-file=no --vid=no --no-resume-playback ~/Downloads/notif.wav & \
    notify-send -i /usr/share/icons/pacman.png "$@"
