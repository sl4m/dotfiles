#!/bin/bash

cd /tmp || exit
test_dir="test$(head -3 /dev/urandom |tr -dc '[:digit:]'|cut -c -10)"
mkdir "$test_dir" && cd "$_" || exit

touch file{0..20}
find . -type f -exec sh -c 'openssl rand -base64 20 > {}' \;

echo "Test environment set-up!"
$SHELL
trash-put /tmp/"$test_dir"
echo "Test Dir removed!"
