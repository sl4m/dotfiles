#!/bin/sh
# Usage: ./generate_random_favico.sh <PICS_DIR/PIC_DIR>
# Example: ./generate_rand_favico.sh  /home/user/Pictures

if [ $1 ];then
    convert -resize x32 -gravity center -crop 32x32+0+0 -flatten -background Transparent -colors 256 $1 favicon.ico && sxiv -p ./favicon.ico
else
    convert -resize x32 -gravity center -crop 32x32+0+0 -flatten -background Transparent -colors 256 $(ls  "$1" |shuf -n1) favicon.ico && sxiv -p ./favicon.ico
fi
