#!/bin/sh

WALL_DIR="$HOME/Pictures/wallpapers"

setbg() {
    height=$(identify -format "%h" "$1")
    width=$( identify -format "%w" "$1")

    if [ $height -ge 1080 ] && [ $width -ge 1920 ]; then
        hsetroot -fill "$1" >/dev/null
    else
        (echo center ; echo fill; echo extend; echo tile; echo cover;echo full) |dmenu -p " Set Background " | xargs -I{} hsetroot -{} "$1" >/dev/null
    fi
}

if [ "$1" ]; then
    setbg "$1"
else
    setbg "$(find $WALL_DIR |shuf -n1)"
fi
