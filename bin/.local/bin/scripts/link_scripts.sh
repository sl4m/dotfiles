#!/bin/sh
set -eux

# Link only nedded scripts to PATH

awk "!/^#/{print \"ln -sf /home/$USER/.local/bin/scripts/\"\$1\" /usr/local/bin/\"\$2}"  bin_aliases.txt | sudo bash
