#!/usr/bin/env sh
set -eu

TARGET_DIR="${1:-.}"

fd . "$TARGET_DIR" | fzf -m --bind 'esc:accept'
