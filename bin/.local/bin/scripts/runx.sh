#!/bin/sh

# Grab XAUTHORITY
xauthority() {
  ps -C xinit -f --no-header | sed 's/.*-auth //; s/ -[^ ].*//'
}

user=$(w -h | cut -d ' ' -f 1)
user_id=$(id -u "$user")

if [ -z "$1" ]; then
  printf "usage: runx <command> [argument, ...]\n" >&2
  exit 64
fi

if [ "$(id -ru)" = "0" ]; then
  cmd="$@ "
  set -- su "$user" -c "$cmd"
fi

DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$user_id/bus" DISPLAY=:0 \
XAUTHORITY="$(xauthority)" XDG_RUNTIME_DIR="/run/user/$user_id" "$@"
