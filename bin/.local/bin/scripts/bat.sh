#!/usr/bin/env bash
CRITICAL_LEVEL="${CRITICAL_LEVEL:-10}"
CRITICAL_ICON="${CRITICAL_ICON:-"/usr/share/icons/Adwaita/24x24/legacy/battery-caution.png"}"

LOW_LEVEL="${LOW_LEVEL:-20}"
LOW_ICON="${LOW_ICON:-"/usr/share/icons/Adwaita/24x24/legacy/battery-low.png"}"

battery_level="$(acpi -b | grep -v "Charging" | grep -P -o '([0-9]+(?=%))')"

if [[ -z "$battery_level" ]]; then
	exit 0
fi

if [[ "$battery_level" -le "$CRITICAL_LEVEL" ]]; then
	notify-send -i "$CRITICAL_ICON" -u critical "Battery Critical" "Battery level is ${battery_level}%"
	exit 0
fi

if [[ "$battery_level" -le "$LOW_LEVEL" ]]; then
	notify-send -i "$LOW_ICON" -t 15000 -u normal "Battery Low" "Battery level is ${battery_level}%"
	exit 0
fi
