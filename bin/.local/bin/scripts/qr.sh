#!/bin/sh

qrencode -t PNG -s 11 -m 1 -o /tmp/qrencoded.png "$@" && setsid sxiv -s f -bf "/tmp/qrencoded.png" &
sleep 0.1
xdotool key a
