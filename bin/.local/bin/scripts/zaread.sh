#!/bin/bash
## zaread - a simple script created by paoloap.

# default variables
zadir="$HOME"'/.cache/zaread/'
reader="zathura"

# if ~/.zaread doesn't exist, we create it.
if [[ ! -d "$zadir" ]]; then
  mkdir "$zadir"
  mkdir "$zadir"cksum
fi

# if no arguments exit.
if [[ -z $@ ]]; then exit 1; fi

# if zathura is not installed, we force the user to choose a pdf reader
# after three wrong commands, the script exits 1
# if the user inserts a command that exists but is not a pdf reader then... then fuck him.
counter=0
while [[ -z `command -v "$reader"` ]]; do
  if [ $counter -gt 3 ]; then exit 1; fi
  let counter+=1
  echo "Seems that you don't have zathura installed. Please choose an installed PDF reader:"
  read reader
done
echo "We'll read PDF with $reader."


## create position and file variables ##

# complete file name (path excluded):
file=`echo "$@" | rev | cut -d'/' -f1 | rev`

# complete directory path:
# if it has been inserted absolute path ($@ starts with '/')
if [[ $@ =~ ^/ ]]; then
  directory=`echo "$@" | rev | cut -d'/' -f2- | rev`"/"
# else (relative path inserted)
else
  dir=`pwd`"/"`echo "$@" | sed 's|.[^/]*$||'`"/"
  directory=`echo "$dir" | sed 's|//|/|'`
fi
echo "$directory""$file"

# if file type is pdf, then just read the file
if [[ `file "$directory""$file" | cut -d':' -f2 | cut -d' ' -f2` == "PDF"  ]]; then
  echo "The file is already in PDF format."
  $reader "$directory""$file"
# else check if you already have its pdf version (if not, create it)
else
  pdffile=`echo "$file" | rev | cut -d'.' -f2- | rev`".pdf"
  check=`cksum "$directory""$file" | awk '{print $1}'`
  # if pdf version hasn't ever been created, or it changed, then
  # make conversion and store the checksum.
  if [[ ( ! -f "$zadir$pdffile" ) || ( ! "$check" == `cat "$zadir"cksum/"$file".check` ) ]]; then
    # if it's a mobi file, then convert it to epub (the command depends on calibre)
    if [[ "$file" =~ ^.*\.mobi$ ]]; then
      ebook-converter "$directory""$file" "$directory"`echo "$file" | sed 's/mobi$/epub/'`
    else
      libreoffice --convert-to pdf "$directory""$file" --headless --outdir "$zadir"
    fi
    echo "$check" > "$zadir"cksum/"$file".check
  fi
  $reader "$zadir$pdffile"
fi

# Add this v to /usr/share/applications/zaread.desktop for xdg compatibility

#[Desktop Entry]
#Terminal=false
#Name=Zaread
#Type=Application
#Icon=zaread
#Exec=zaread %f
#Categories=Office;WordProcessor;Viewer;
#MimeType=application/vnd.oasis.opendocument.text;application/vnd.oasis.opendocument.text-template;application/vnd.oasis.opendocument.text-web;application/vnd.oasis.opendocument.text-master;application/vnd.oasis.opendocument.text-master-template;application/vnd.sun.xml.writer;application/vnd.sun.xml.writer.template;application/vnd.sun.xml.writer.global;application/msword;application/vnd.ms-word;application/x-doc;application/x-hwp;application/rtf;text/rtf;application/vnd.wordperfect;application/wordperfect;application/vnd.lotus-wordpro;application/vnd.openxmlformats-officedocument.wordprocessingml.document;application/vnd.ms-word.document.macroEnabled.12;application/vnd.openxmlformats-officedocument.wordprocessingml.template;application/vnd.ms-word.template.macroEnabled.12;application/vnd.ms-works;application/vnd.stardivision.writer-global;application/x-extension-txt;application/x-t602;text/plain;application/vnd.oasis.opendocument.text-flat-xml;application/x-fictionbook+xml;application/macwriteii;application/x-aportisdoc;application/prs.plucker;application/vnd.palm;application/clarisworks;application/x-sony-bbeb;application/x-abiword;application/x-iwork-pages-sffpages;application/x-mswrite;application/x-starwriter;application/vnd.oasis.opendocument.presentation;application/vnd.oasis.opendocument.presentation-template;application/vnd.sun.xml.impress;application/vnd.sun.xml.impress.template;application/mspowerpoint;application/vnd.ms-powerpoint;application/vnd.openxmlformats-officedocument.presentationml.presentation;application/vnd.ms-powerpoint.presentation.macroEnabled.12;application/vnd.openxmlformats-officedocument.presentationml.template;application/vnd.ms-powerpoint.template.macroEnabled.12;application/vnd.openxmlformats-officedocument.presentationml.slide;application/vnd.openxmlformats-officedocument.presentationml.slideshow;application/vnd.ms-powerpoint.slideshow.macroEnabled.12;application/vnd.oasis.opendocument.presentation-flat-xml;application/x-iwork-keynote-sffkey;
#GenericName=Word Processor
#Keywords=Document;OpenDocument Text;Microsoft Word;Microsoft Works;Lotus WordPro;OpenOffice Writer;CV;odt;doc;docx;rtf;Slideshow;Slides;OpenDocument Presentation;Microsoft PowerPoint;Microsoft Works;OpenOffice Impress;odp;ppt;pptx;
