#!/bin/bash

title=$(playerctl --player=ncspot metadata title| tr -dc '[:alnum:]\n\r' | tr '[:upper:]' '[:lower:]')
artist=$(playerctl --player=ncspot metadata xesam:albumArtist | sed 's/,.*//g' | tr -dc '[:alnum:]\n\r' | tr '[:upper:]' '[:lower:]')

if [[ -z $title || -z $artist ]];then
    echo "Couldnt Get the title/artist"
    exit 1
fi

echo [*] Trying https://www.azlyrics.com/lyrics/$artist/$title.html
echo ""

# Them: NOOO! YOU CANNOT USE REGEX TO PARSE HTML!!
# Me: HaHah Regex goes brrrr
curl -Ls -H "User-Agent: Mozilla/5.0 Firefox/70.0" "https://www.azlyrics.com/lyrics/$artist/$title.html" \
    |sed -n '/^<!-- Usage/,/^<!-- MxM/p' \
    |sed -e 's/<.*>//g' -e '/^\s$/d' -e 's/\s\+$//'
