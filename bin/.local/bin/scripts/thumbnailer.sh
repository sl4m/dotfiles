#!/usr/bin/env sh
# open dir containing images and videos in sxiv

DIR=${1:-$PWD}
DIR=$(realpath "$DIR")


THUMB_DIR="$(mktemp -d)"
echo "[*] Using $DIR"
echo "[*] Saving Under $THUMB_DIR"

fd  . -tf -emp4 -emov -emkv -ewebm -ewmv -em4v  "$DIR" -x ffmpegthumbnailer -t 25% -c png -s 720 -i {} -o "$THUMB_DIR/thumb.{/.}.png" 2>/dev/null

files=$(sxiv -op "$THUMB_DIR/" | xargs -I% basename % | sed -e 's/.png//g' -e 's/thumb\.//g')

echo "$files" |parallel mpv "$DIR/{}*"

rm -r "$THUMB_DIR"
