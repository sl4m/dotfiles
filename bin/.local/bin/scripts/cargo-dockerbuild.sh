#!/bin/sh
# Builds the release binary of your working dir project in a rust docker container
# Usage: ./cargo-dockerbuild <RUST_IMAGE:TAG>
# Ex: ./cargo-dockerbuild rust:slim-buster
set -eu

pname=$(basename "$PWD")

docker run --rm --user 1000:998 -v "$PWD":/usr/src/"$pname" -w /usr/src/"$pname" "$1" cargo build --release "$2"
