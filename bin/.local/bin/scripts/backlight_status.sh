#/bin/bash

# https://pbrisbin.com/tags/linux/

case "$1" in
    "i")
        xbacklight -inc 5;;
    "d")
        xbacklight -dec 5;;
esac

level=$(xbacklight -get)

# we use a fifo to buffer the repeated commands that are common with 
# volume adjustment
pipe='/tmp/brightness_pipe'

# define some arguments passed to dzen to determine size and color.
dzen_args=( -xs 1 -tw 200 -h 25 -x 1690 -y 40 -bg '#101010' -fn 'Inconsolata Nerd Font Mono:style=Regular:size=16' )
#dzen_args=( -xs 2 -p -tw 200  -h 25 -y '35' -x '1690' -fn 'Inconsolata Nerd Font Mono:style=Regular:size=14' -ta c -sa c )

# similarly for gdbar
gdbar_args=( -l '  ' -w 180 -h 7 -fg '#800060' -bg '#404040' )
#gdbar_args=( -w 200 -max 100 -min 0 -l ' Brightness ' -bg '#777777' -fg '#00ff00' -ss '1' )

# spawn dzen reading from the pipe (unless it's in mid-action already).
if [[ ! -e "$pipe" ]]; then
  mkfifo "$pipe"
  (dzen2 "${dzen_args[@]}" < "$pipe"; rm -f "$pipe") &
fi

# send the text to the fifo (and eventually to dzen)
# and send gdbar an optional "upper limit" argument after 1st }
(echo ${level/.*/} | gdbar "${gdbar_args[@]}"; sleep 1) >> "$pipe"
