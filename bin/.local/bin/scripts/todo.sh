#!/bin/sh

[ ! -e ~/todo.md ] && echo [!] no ~/todo.md file found! please make one && exit 1

if [ "$1" = "-e" ];then
    $EDITOR ~/todo.md
else
    #bat --theme Dracula -p ~/todo.md
    #mdcat -lp ~/todo.md
    lowdown -t term ~/todo.md |less -R
fi
