#!/bin/sh
## Returns the top 20 Downloads sorted search results from crates.io
# ./cargo-ssearch.sh <CRATE>

query=$(echo "$@" | jq -rR @uri)
echo "----searching for $query-----"

curl -fsSL "https://crates.io/api/v1/crates?page=1&per_page=20&q=$query&sort=downloads" \
    | jq '.crates | .[] | "["+.id+" "+.newest_version+"]:   ("+.description+")    [ Downloads:"+(.downloads|tostring)+"  [] https://crates.io/crates/"+.id'

echo "-----------------------------"

