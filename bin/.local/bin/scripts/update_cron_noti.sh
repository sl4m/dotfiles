#!/bin/sh

pkgs=$(checkupdates)

pkgs_count=$(echo "$pkgs" |wc -l)

noti="<b>$pkgs_count</b> Packages to Update"

echo "$pkgs" | grep -q "^linux.*$" && noti="$noti\n<b>including Kernel update</b>"

notify-audio -i /usr/share/icons/pacman.png '[Pacman]' "$noti"
