#!/bin/sh
set -eu

script_name=$(fd -u --regex '^([^.]+)$' -t f -t l --maxdepth 1 $HOME/.local/bin/scripts/ -x echo {/} | \
    dmenu -nf "#4e8278"  -sf "#dfbf87"  -nb "#3A3935" -sb  "#1B1B1B" -l 5 -p " RunScript")

$HOME/.local/bin/scripts/$script_name
