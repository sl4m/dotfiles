let mapleader = ","
syntax on
set hidden
set nocompatible
set showcmd
set autoread
set hlsearch
set incsearch
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set expandtab
set ignorecase
set smartcase
set fileformat=unix
set encoding=utf-8
set number relativenumber
set omnifunc=syntaxcomplete#Complete
set wildmode=longest,list,full
map q: :q
set so=10
let g:loaded_perl_provider=0
let g:loaded_ruby_provider=0
let g:loaded_node_provider=0

" tab handling
nnoremap <silent> <c-t> :tabnew<cr>
nnoremap <silent> <c-k> :tabn<cr>
nnoremap <silent> <c-j> :tabp<cr>

" Select All
nnoremap <C-a> ggVG
" Remove trailing spaces
autocmd BufWritePre * %s/\s\+$//e

" format json
"au BufNewFile,BufRead,BufReadPost,BufWritePost *json :%!jq .

"autocmd BufWritePost *.rs :call :CocActionAsync('format')

autocmd BufNewFile,BufRead *.hbs set syntax=html
autocmd BufNewFile,BufRead justfile,.justfile set syntax=just

" Edit and Reload [n]Vimrc
nnoremap <Leader>vc :source ~/.config/nvim/init.vim<CR>:echo "Reloaded vimrc"<CR>

" Sort
nnoremap <leader>s :%!sort -u --version-sort<CR>

" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

autocmd BufWritePost *.lua !lua5.1 %

" Save as root using sudo
cmap w!! w !sudo tee > /dev/null %


" Copy/Paste
"set clipboard=unamedplus
vmap <leader>y "+y
nmap <leader>y "+y
vmap <leader>p c<ESC>"+p
nmap <leader>p "+p
vmap <leader>x "+xi

let b:myLang=0
let g:myLangList=["nospell","en_us"]
function! ToggleSpell()
  let b:myLang=b:myLang+1
  if b:myLang>=len(g:myLangList) | let b:myLang=0 | endif
  if b:myLang==0
    setlocal nospell
  else
    execute "setlocal spell spelllang=".get(g:myLangList, b:myLang)
  endif
  echo "spell checking language:" g:myLangList[b:myLang]
endfunction

nmap <silent> <F5> :call ToggleSpell()<CR>

" add new empty line under current
nmap <c-n> o<esc>Dk
" move text under
nmap <leader>j i<cr><esc>
" unwrap?
map Q gqq
" search for selected text in v mode
vnoremap <c-s> "hy:%s/<C-r>h//gc<left><left><left>
" switch to i-beam when in insertmode
if has("autocmd")
  au vimenter,insertleave * silent execute '!echo -ne "\e[2 q"' | redraw!
  au InsertEnter,InsertChange *
    \ if v:insertmode == 'i' |
    \   silent execute '!echo -ne "\e[6 q"' | redraw! |
    \ elseif v:insertmode == 'r' |
    \   silent execute '!echo -ne "\e[4 q"' | redraw! |
    \ endif
  au VimLeave * silent execute '!echo -ne "\e[ q"' | redraw!
endif

"autocmd Filetype java set makeprg=javac\ %
"set errorformat=%A%f:%l:\ %m,%-Z%p^,%-C%.%#
"map <leader>jc :make<Return>:copen<Return>
"map <leader>jj :!clear && java  %:r<CR>
"autocmd BufWritePost *java !javac %

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/Vundle.vim
call vundle#begin()
Plugin 'neoclide/coc.nvim'
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-surround' "cs, ys,
Plugin 'airblade/vim-rooter'
Plugin 'junegunn/rainbow_parentheses.vim'
Plugin 'mcchrish/nnn.vim'
Plugin 'preservim/nerdcommenter'
Plugin 'preservim/nerdtree'
Plugin 'gregsexton/MatchTag'
Plugin 'honza/vim-snippets'
Plugin 'Raimondi/delimitMate' "Auto completion for brackets,quotes,etc
Plugin 'itchyny/lightline.vim'
Plugin 'itchyny/vim-gitbranch'
Plugin 'wfxr/minimap.vim'
"Plugin 'rust-lang/rust.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-syntastic/syntastic'
Plugin 'vimwiki/vimwiki'
Plugin 'bluz71/vim-moonfly-colors'
Plugin 'sedm0784/vim-you-autocorrect'
Plugin 'NoahTheDuke/vim-just'
call vundle#end()            " required
filetype plugin indent on    " required

let g:vim_you_autocorrect_disable_highlighting = 1
let g:vimwiki_list = [{'path': '~/Documents/wiki/', 'ext': '.md'}]


"light status bar configuration and timing
set timeoutlen=3000 ttimeoutlen=100
set updatetime=300
set laststatus=2
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'moonfly',
      \ 'active': {
      \   'left': [ [ 'aa', 'mode', 'paste' ],
      \             ['gitbranch', 'readonly', 'filename', 'modified' ] ],
      \   'right': [ [ 'lineinfo' ],
      \              [ 'percent' ],
      \              [ 'vim_logo','fileformat', 'fileencoding', 'filetype', 'charvaluehex' ] ]
      \ },
      \ 'component':{'vim_logo':''},
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ }
      \ }


let g:rainbow#max_level = 16
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]

" nnn file manager
let g:nnn#command = 'NNN_FIFO="/tmp/nnn_vim.fifo" NNN_TRASH=1 nnn -d'
" Disable default mappings
let g:nnn#set_default_mappings = 0
" Set custom mappings
nnoremap <leader>n :NnnPicker %:p:h<CR>
nnoremap <silent> <leader>N :NnnExplorer<CR>
let g:nnn#layout = { 'window': { 'width': 0.9, 'height': 0.6, 'highlight': 'Debug' } }
let g:nnn#action = {
      \ '<c-t>': 'tab split',
      \ '<c-x>': 'split',
      \ '<c-v>': 'vsplit' }

" NerdTree
nnoremap <leader>b :NERDTreeToggle<CR>


"NerdComment toggle
map <C-c> ,c<space>

" rust.vim stuff
"let g:rustfmt_autosave = 1
map <leader>m :MinimapToggle<CR>
let g:minimap_auto_start = 0
let g:minimap_width = 10
let g:minimap_highlight_search=1
let g:minimap_git_colors = 1


augroup taste_the_rainbow
  autocmd!
  autocmd FileType rust,java,javascript,python,c RainbowParentheses
augroup END

" coc mappings
set signcolumn=number
command! -nargs=0 F :call CocActionAsync('format')
nmap <leader>k :CocCommand rust-analyzer.openDocs<CR>

inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#confirm() : "\<Tab>"

imap <C-j> <Plug>(coc-snippets-expand-jump)
map <leader>jg :CocAction<CR>
map <leader>f :FZF<CR>

" themes and colorscheme:

"set t_Co=256
set termguicolors
colorscheme moonfly
let g:moonflyVirtualTextColor = v:true
let g:moonflyTransparent = v:true
