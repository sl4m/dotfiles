#!/usr/bin/env sh

# Description: Open images in hovered  directory and thumbnails
#              open hovered image in sxiv and browse other images in the directory
# Dependencies: sxiv (https://github.com/muennich/sxiv)
#
# Shell: POSIX compliant
# Author: Arun Prakash Jana

abspath() {
    case "$1" in
        /*) printf "%s\n" "$1";;
        *)  printf "%s\n" "$PWD/$1";;
    esac
}

listimages() {
    imgs=$(find -L "$(dirname "$target")" -maxdepth 1 -type f -iregex '.*\(jpe?g\|bmp\|webp\|png\|gif\)$'|wc -l)

    if [ $imgs -ne 0 ];then
        find -L "$(dirname "$target")" -maxdepth 1 -type f -iregex '.*\(jpe?g\|bmp\|webp\|png\|gif\)$' -print0 | sort -z
    else
        find -L "$(dirname "$target")"  -maxdepth 1 -type f  |  xargs -I % file --mime-type % |grep image | sed 's/\:\simage.*$//g'  | tr  '\n' '\0' |sort -z
    fi
}

view_dir() {
    target="$(abspath "$2")"
    count="$(listimages | grep -a -m 1 -ZznF "$target" | cut -d: -f1)"

    if [ -n "$count" ]; then
        if [ "$1" = 'sxiv' ]; then
            listimages | xargs -0 "$1" -s f -pan "$count" --
        fi
    else
        shift
        "$1" -- "$@" # fallback
    fi
}

if [ -z "$1" ] || ! [ -s "$1" ]; then
    printf "empty file"
    read -r _
    exit 1
fi

if which sxiv >/dev/null 2>&1; then
    if [ -f "$1" ]; then
        view_dir sxiv "$1" >/dev/null 2>&1 &
    elif [ -d "$1" ] || [ -h "$1" ]; then
        sxiv  -paqt "$1" >/dev/null 2>&1 &
    fi
else
    printf "Please install sxiv"
    read -r _
    exit 2
fi
